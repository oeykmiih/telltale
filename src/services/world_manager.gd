extends Node
class_name WorldManager

const grid_size: int = 64
const grid: Vector2 = Vector2(grid_size, grid_size)
static var map: Node
static var map_bounds: Rect2
static var map_outline: Curve2D
