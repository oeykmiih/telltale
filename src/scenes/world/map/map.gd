extends Node2D

func _ready():
    %Canvas.position -= WorldManager.grid / 2
    WorldManager.map = %Canvas

    var bounds: Rect2 = %Canvas.get_rect().grow(-WorldManager.grid_size/2.0)
    WorldManager.map_bounds = bounds

    var outline = Curve2D.new()
    outline.add_point(bounds.position)
    outline.add_point(Vector2(bounds.end.x, bounds.position.y))
    outline.add_point(bounds.end)
    outline.add_point(Vector2(bounds.position.x, bounds.end.y))
    outline.add_point(bounds.position)
    WorldManager.map_outline = outline
    return null

func _on_item_rect_changed():
    WorldManager.map_bounds = %Canvas.get_rect()
    return null
