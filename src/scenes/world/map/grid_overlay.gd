extends TextureRect

@export var color : Color = Color(0.0, 0.0, 0.0, 1.0)
@export var width : float = 1.0

var viewport : Viewport
var camera : Camera2D

var zoom : Vector2

func _ready():
    viewport = get_viewport()
    camera = viewport.get_camera_2d()
    return null

func _process(_delta):
    queue_redraw()

func _draw():
    zoom = camera.zoom

    var topleft : Vector2 = position
    var bottomright : Vector2 = topleft + size

    var y : float = ceil(topleft.y / WorldManager.grid.y) * WorldManager.grid.y
    var n_y : float = bottomright.y / WorldManager.grid.y + 1
    for i in range(0, n_y):
        draw_line(Vector2(topleft.x , y), Vector2(bottomright.x, y), color, width/zoom.y)
        y += WorldManager.grid.y

    var x : float = ceil(topleft.x / WorldManager.grid.x) * WorldManager.grid.x
    var n_x : float = bottomright.x / WorldManager.grid.x + 1
    for i in range(0, n_x):
        draw_line(Vector2(x , topleft.y), Vector2(x, bottomright.y), color, width/zoom.x)
        x += WorldManager.grid.x
    return null
