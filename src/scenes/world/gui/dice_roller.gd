extends GridContainer

func dir(class_instance):
    var output = {}
    var methods = []
    for method in class_instance.get_method_list():
        methods.append(method.name)

    output["METHODS"] = methods

    var properties = []
    for prop in class_instance.get_property_list():
        if prop.type == 3:
            properties.append(prop.name)
    output["PROPERTIES"] = properties

    return output

func _ready():
    for die_size in [4, 6, 8 , 10, 12, 20, 100]:
        for i in range(1, 6):
            var button = Button.new()
            var text : String
            if i == 1:
                text = "d%s" % die_size
            else:
                text = "%s" % i
            button.set_text("%s" % text)
            button.mouse_filter = Control.MOUSE_FILTER_PASS
            button.size_flags_horizontal = Control.SIZE_EXPAND_FILL
            button.theme_type_variation = "FlatButton"

            button.pressed.connect(roll_dice.bind(die_size, i))

            add_child(button)
    return null

func roll_dice(die_size : int, n : int = 1):
    var results : Array = []
    for i in range(n):
        results.append(randi_range(1, die_size))

    printt(die_size, results)
    return null
