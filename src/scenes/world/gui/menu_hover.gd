extends Control

@export var group : ButtonGroup

var open_panel : Node
var is_sticky : bool
var in_child : bool
var in_parent : bool

func _ready():
    for button in group.get_buttons():
        var panel = button.get_child(0)
        button.mouse_entered.connect(mouse_entered_group.bind(panel))
        button.pressed.connect(pressed_group.bind(panel))
        button.mouse_exited.connect(mouse_exited_group.bind(panel))
        panel.mouse_entered.connect(mouse_entered_panel)
        panel.mouse_exited.connect(mouse_exited_panel.bind(panel))
    return null

func pressed_group(panel):
    if group.get_pressed_button() != null:
        is_sticky = true
        if panel != open_panel:
            open_panel.visible = false
            panel.visible = true
            open_panel = panel
        else:
            open_panel.visible = true
    else:
        is_sticky = false
        open_panel.visible = false
    return null

func mouse_entered_group(panel):
    if not is_sticky:
        in_parent = true
        if open_panel != null:
            open_panel.visible = false

        panel.visible = true
        open_panel = panel
    return null

func mouse_exited_group(panel):
    if not is_sticky:
        in_parent = false
        await get_tree().create_timer(0.1).timeout
        if not in_child:
            panel.visible = false
    return null

func mouse_entered_panel():
    if not is_sticky:
        in_child = true
    return null

func mouse_exited_panel(panel):
    if not is_sticky:
        in_child = false
        if not in_parent:
            panel.visible = false
    return null
