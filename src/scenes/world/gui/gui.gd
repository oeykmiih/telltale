extends CanvasLayer

signal reset_view_button_pressed
signal zoom_out_button_pressed
signal zoom_in_button_pressed

func _on_zoom_out_button_pressed():
    zoom_out_button_pressed.emit()
    return null

func _on_zoom_in_button_pressed():
    zoom_in_button_pressed.emit()
    return null

func _on_reset_view_button_pressed():
    reset_view_button_pressed.emit()
    return null
