extends Camera2D

const MIN_ZOOM : float = 0.25
const MAX_ZOOM : float = 1.5
const ZOOM_STEP : float = 0.25
enum ZOOM_TYPE {
    MOUSE,
    VIEW,
}

var zoom_default : float
var zoom_target : float = 1.0

func _ready():
    var viewport = get_viewport()
    update_default_zoom(viewport)
    viewport.size_changed.connect(update_default_zoom.bind(viewport))
    view_reset()
    return null

func update_default_zoom(viewport):
    zoom_default = snapped(viewport.size.y / WorldManager.map.size.y, ZOOM_STEP) - ZOOM_STEP
    return null

func center_on_target(target):
    position = target.global_position + (target.size / 2)
    return null

func view_reset():
    center_on_target(WorldManager.map)
    _zoom(zoom.x > zoom_default, ZOOM_TYPE.VIEW, abs(zoom.x - zoom_default))
    return null

func _zoom(zoom_out : bool = false, target : ZOOM_TYPE = ZOOM_TYPE.MOUSE, amount : float = ZOOM_STEP):
    if amount > 0:
        if zoom_out == false:
            zoom_target = min(zoom_target + amount, MAX_ZOOM)
        else:
            zoom_target = max(zoom_target - amount, MIN_ZOOM)

        match target:
            ZOOM_TYPE.MOUSE:
                var mouse_pos : Vector2 = get_global_mouse_position()
                zoom = lerp(zoom, zoom_target * Vector2.ONE, 1.0)
                var new_mouse_pos : Vector2 = get_global_mouse_position()
                position += mouse_pos - new_mouse_pos
            ZOOM_TYPE.VIEW:
                zoom = lerp(zoom, zoom_target * Vector2.ONE, 1.0)
    return null

func _input(event : InputEvent):
    if event is InputEventMouseButton:
        if event.is_pressed():
            if event.button_index == MOUSE_BUTTON_WHEEL_UP:
                _zoom()
            if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
                _zoom(true)

    if event is InputEventMouseMotion:
        if event.button_mask == MOUSE_BUTTON_MASK_MIDDLE:
            position -= event.relative * (1.0 / zoom_target)
    return null

func _on_gui_reset_view_button_pressed():
    view_reset()
    return null

func _on_gui_zoom_in_button_pressed():
    _zoom(false, ZOOM_TYPE.VIEW)
    return null

func _on_gui_zoom_out_button_pressed():
    _zoom(true, ZOOM_TYPE.VIEW)
    return null
