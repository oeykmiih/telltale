extends Node2D

var disable_grid: bool = false
var active: bool = false

var new_pos: Vector2
var dif: Vector2

func _input(event):
    if event is InputEventKey:
        match event.keycode:
            KEY_ALT:
                disable_grid = event.pressed
            _:
                pass
    return null

func _process(_delta):
    if active:
        new_pos = get_global_mouse_position()
        new_pos =  new_pos

        if not disable_grid:
            new_pos = new_pos.snapped(WorldManager.grid)

        if not WorldManager.map_bounds.has_point(new_pos):
            new_pos =  WorldManager.map_outline.get_closest_point(new_pos)

        self.global_position = new_pos
    return null

func _on_mouse_region_button_down():
    dif = get_local_mouse_position()
    active = true
    return null

func _on_mouse_region_button_up():
    active = false
    return null
